using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes;

public class MakeModel : MonoBehaviour
{

    public TextMesh remainText;
    public TextMesh printTimeText;
    // Start is called before the first frame update
    public GameObject PrinterOrigin;
    public gcode_parser gp;
    public bool startCalibration = false;
    private bool startPrinting = false;
    private float printTime = 0;
    private float printStartTime = 0;
    private bool printTimeSet = false;
    public float maxSpeed;
    public float audioLength;
    public Vector3 localXPosStop;
    public Vector3 localYPosStop;
    public Vector3 localZPosStop;


    public Vector3 FinallocalXPosStop;
    public Vector3 FinallocalYPosStop;
    public Vector3 FinallocalZPosStop;

    private Vector3 worldXPosStop;
    private Vector3 worldYPosStop;
    private Vector3 worldZPosStop;

    private float journeyLengthX;
    private float journeyLengthY;
    private float journeyLengthZ;

    public GameObject bed;
    public GameObject Zgantry;
    public GameObject Carriage;

    private Vector3 bedStart;
    private Vector3 ZgantryStart;
    private Vector3 CarriageStart;

    private GameObject MovePoint;

    private int currentMove = -1;

    private bool moveSet = false;
    private bool printDone = false;

    // Movement speed in units per second.
    public float speed = 1.0F;
    // Time when the movement started.
    private float startTime;

    private bool timeSet = false;
    private bool calibrationDone = false; 
    private Vector3 currentStartPosBed;
    private Vector3 currentEndPosBed;
    private Vector3 currentStartPosZgantry;
    private Vector3 currentEndPosZgantry;
    private Vector3 currentStartPosCarriage;
    private Vector3 currentEndPosCarriage;

    private Vector3 FinalStartPosBed;
    private Vector3 FinalStartPosZgantry;
    private Vector3 FinalStartPosCarriage;

    private float offsetY;

    public GameObject printContainer;
    public GameObject nozzlePoint;

    private float progressVar;
    private bool extruding = false;

    // public GameObject LineObject;

    public GameObject PlObject;
    public Polyline PlComponent;
    private Polyline currentPolyLine;
    private GameObject currentPolyLineObject;

    private Color currentColor;
    public Texture2D rainbowSample;

    private bool nextExtruding = false;

    public Rectangle progressBar;
    private float t;
    void Start()
    {
        gp = GetComponent<gcode_parser>();
        // Keep a note of the time the movement started.
        //startTime = Time.time;

        bedStart = bed.transform.localPosition;
        ZgantryStart = Zgantry.transform.localPosition;
        CarriageStart = Carriage.transform.localPosition;

        offsetY = Zgantry.transform.position.y - Carriage.transform.position.y;


    }

    // TO DO 
    // Create object on bed for print.
    // if extrude is true
    // Change position of end point until end of move then leave it.
    // Start point can be set in the MoveTo funciton (world position of NozzlePoint)
    // Then end point can be set in update based on the NozzlePoint position.

    // Update is called once per frame
    void Update()
    {
        if (printTimeSet)
        {       
            printTime = Time.time - printStartTime;
            int minutes = Mathf.FloorToInt(printTime / 60F);
            int seconds = Mathf.FloorToInt(printTime - minutes * 60);
            string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);

            int minutesRemain = Mathf.FloorToInt((audioLength - printTime) / 60F);
            int secondsRemain = Mathf.FloorToInt((audioLength - printTime) - minutesRemain * 60);
            string remainTime = string.Format("{0:0}:{1:00}", minutesRemain, secondsRemain);
            if (printTime <= audioLength)
            {
                printTimeText.text = niceTime;
                remainText.text = remainTime;
            }
        }
        if (startCalibration)
        {
            if(!printTimeSet)
            {
                printStartTime = Time.time;
                printTimeSet = true;
            }
            if(!timeSet)
            {
                //startTime = Time.time;
                t = 0;
                timeSet = true;
            }
            // Distance moved equals elapsed time times speed..
            //float t = (Time.time - startTime) * speed;
            t += (Time.deltaTime) * speed;
            //Debug.Log(t);
            // Fraction of journey completed equals current distance divided by total distance.
            //float fractionOfJourneyX = distCovered / journeyLengthX;
            //float fractionOfJourneyY = distCovered / journeyLengthY;
            //float fractionOfJourneyZ = distCovered / journeyLengthZ;

            // Set our position as a fraction of the distance between the markers.
            // Move X to stop
            bed.transform.localPosition = Vector3.Lerp(bedStart, localXPosStop, t);
            // Move Y to stop
            Zgantry.transform.localPosition = Vector3.Lerp(ZgantryStart, localYPosStop, t);
            // Move Z to stop
            Carriage.transform.localPosition = Vector3.Lerp(CarriageStart, localZPosStop, t);
            //if (fractionOfJourneyX > 1 && fractionOfJourneyY > 1 && fractionOfJourneyZ > 1)
            if(t > 1)
            {
                // These are the vectors that the movement vector needs to be added to.
                worldXPosStop = bed.transform.position;
                worldYPosStop = Zgantry.transform.position;
                worldZPosStop = Carriage.transform.position;
                startCalibration = false;
                calibrationDone = true;
                Vector3 calibrationOrigin = new Vector3(worldXPosStop.x, worldYPosStop.y, worldZPosStop.z);
                Instantiate(PrinterOrigin, calibrationOrigin, Quaternion.identity);
                MovePoint = GameObject.Find("MoveToPoint");
                Invoke("Print", 2f);

            }
        }
        if(startPrinting && calibrationDone && !printDone)
        {
            if (!moveSet)
            {
                MoveTo();
            }
            /* if (currentMove > 1 && speed < maxSpeed && progressVar < 0.9)
            {
                speed += (Time.time - printTime) * speedMultiplier;
            }
            if (progressVar > 0.9 && speed > minSpeed)
            {
                if (!apexReached)
                {
                    printTime = Time.time;
                    apexReached = true;
                }
                speed -= (Time.time - printTime) * speedMultiplier;
            }
            */
            //float t = (Time.time - startTime) * speed;
            t += (Time.deltaTime) * speed;
            // Move X to stop
            bed.transform.position = Vector3.Lerp(currentStartPosBed, new Vector3(MovePoint.transform.position.x, bed.transform.position.y, bed.transform.position.z), t);
            // Move Y to stop
            Zgantry.transform.position = Vector3.Lerp(currentStartPosZgantry, new Vector3(Zgantry.transform.position.x, MovePoint.transform.position.y, Zgantry.transform.position.z), t);
            // Move Z to stop
            //offset = Zgantry.transform.position.x - offsetY
            Carriage.transform.position = Vector3.Lerp(currentStartPosCarriage, new Vector3(Carriage.transform.position.x, Zgantry.transform.position.y - offsetY, MovePoint.transform.position.z), t);
            //Carriage.transform.localPosition = new Vector3(Carriage.transform.localPosition.x, 0, Carriage.transform.localPosition.z);



            if (t > 1)
            {
                // EXTRUDE
                if (extruding)
                {
                    //GameObject currentLine = Instantiate(LineObject, nozzlePoint.transform);
                    //currentLine.transform.SetParent(printContainer.transform);
                    currentPolyLine.AddPoint(currentPolyLineObject.transform.InverseTransformPoint(nozzlePoint.transform.position));
                    currentPolyLine.SetPointColor(currentPolyLine.points.Count - 1, currentColor);
                    // CHANGE COLOUR! DO A RAINBOW
                }
                if (!extruding && nextExtruding)
                {
                    currentPolyLineObject = Instantiate(PlObject, nozzlePoint.transform);
                    currentPolyLineObject.transform.SetParent(printContainer.transform);
                    currentPolyLine = currentPolyLineObject.GetComponent<Polyline>();
                }
                //Debug.Log("Move Done!");
                moveSet = false;
            }

        }
        if (printDone)
        {
            speed = 0.25f;
            //float t = (Time.time - startTime) * speed;
            t += (Time.deltaTime) * speed;
            progressBar.Width = 9.9f;
            // Move X to stop
            bed.transform.localPosition = Vector3.Lerp(FinalStartPosBed, FinallocalXPosStop, t);
            // Move Y to stop
            Zgantry.transform.localPosition = Vector3.Lerp(FinalStartPosZgantry, FinallocalYPosStop, t);
            // Move Z to stop
            Carriage.transform.localPosition = Vector3.Lerp(FinalStartPosCarriage, FinallocalZPosStop, t);
            if (t >= 1)
            {
                printTimeSet = false;
            }
        }
    }
    public void startPrint()
    {
        startCalibration = true;
    }

    public void Print()
    {
        startPrinting = true;
        printTime = Time.time;
    }
    public void MoveTo()
    {
        //startTime = Time.time;
        t = 0;
        if (currentMove < gp.xyzArray.Length - 3)
        {
            currentMove++;
            if(currentMove == 1)
            {
                speed = maxSpeed;
            }
            if (gp.commandArray[currentMove] == 1)
            {
                extruding = true;
            }
            else
            {
                extruding = false;
                if (gp.commandArray[currentMove+1] == 1)
                {
                    nextExtruding = true;
                }
                else
                {
                    nextExtruding = false;
                }
            }
            int progressInt = Mathf.CeilToInt(remap(currentMove, 0, gp.xyzArray.Length) * 100);
            


            if (progressInt < 100 && progressInt > 0)
            {
                currentColor = rainbowSample.GetPixel(1, progressInt);
                progressBar.Width = (float)(progressInt / 10);
            }

            //// Set current start position
            currentStartPosBed = bed.transform.position;
            currentStartPosZgantry = Zgantry.transform.position;
            currentStartPosCarriage = Carriage.transform.position;

            Vector3 newPos = new Vector3(gp.xyzArray[currentMove].y / 1000, gp.xyzArray[currentMove].z / 1000, gp.xyzArray[currentMove].x / 1000);
            MovePoint.transform.localPosition = newPos;

            // Move set
            moveSet = true;
        }
        else
        {
            Debug.Log("Print done!");
            printDone = true;
            FinalStartPosBed = bed.transform.localPosition;
            FinalStartPosZgantry = Zgantry.transform.localPosition;
            FinalStartPosCarriage = Carriage.transform.localPosition;
        }

    }

 
    float remap(float rawValue, float min, float max)
    {
        float scaledValue = (rawValue - min) / (max - min);
        return scaledValue;
    }

}
