using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class HandAnimator : MonoBehaviour {
    public float Speed = 20f;

    private Animator _animator;

    private float _pinkyTarget;
    private float _ringTarget;
    private float _middleTarget;
    private float _thumbTarget;
    private float _indexTarget;

    private float _pinkyCurrent;
    private float _ringCurrent;
    private float _middleCurrent;
    private float _thumbCurrent;
    private float _indexCurrent;
    
    // Start is called before the first frame update
    void Start() {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        AnimateHand();
    }


    public void SetPinky(float selectActionValue) {
        _pinkyTarget = selectActionValue;
    }

    public void SetRing(float selectActionValue) {
        _ringTarget = selectActionValue;
    }

    public void SetMiddle(float selectActionValue) {
        _middleTarget = selectActionValue;
    }

    public void SetThumb(float activateActionValue) {
        _thumbTarget = activateActionValue;
    }

    public void SetIndex(float activateActionValue) {
        _indexTarget = activateActionValue;
    }

    private void AnimateHand() {
        if (!Mathf.Approximately(_pinkyCurrent,_pinkyTarget)) {
            _pinkyCurrent = Mathf.MoveTowards(_pinkyCurrent, _pinkyTarget, Time.deltaTime * Speed);
            _animator.SetFloat("Pinky", _pinkyCurrent);
        }        
        if (!Mathf.Approximately(_ringCurrent,_ringTarget)) {
            _ringCurrent = Mathf.MoveTowards(_ringCurrent, _ringTarget, Time.deltaTime * Speed);
            _animator.SetFloat("Ring", _ringCurrent);
        }        
        if (!Mathf.Approximately(_middleCurrent, _middleTarget)) {
            _middleCurrent = Mathf.MoveTowards(_middleCurrent, _middleTarget, Time.deltaTime * Speed);
            _animator.SetFloat("Middle", _middleCurrent);
        }        
        if (!Mathf.Approximately(_thumbCurrent, _thumbTarget)) {
            _thumbCurrent = Mathf.MoveTowards(_thumbCurrent, _thumbTarget, Time.deltaTime * Speed);
            _animator.SetFloat("Thumb", _thumbCurrent);
        }        
        if (!Mathf.Approximately(_indexCurrent,_indexTarget)) {
            _indexCurrent = Mathf.MoveTowards(_indexCurrent, _indexTarget, Time.deltaTime * Speed);
            _animator.SetFloat("Index", _indexCurrent);
        }        
    }
    
}
