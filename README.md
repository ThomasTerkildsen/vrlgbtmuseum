# VRLGBTMuseum

A VR museum to platform the stories of LGBTQIA+ people. The museum contains 3D photogrammetry/LiDAR scans of objects of personal significance, chosen by people in the LGBTQIA+ community - and showcases artwork by queer artists all over the world.